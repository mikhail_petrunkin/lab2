#pragma once
#include <iostream>
#include <cmath>
class Rnd
{
private:
	float *m;
	int n;
	Rnd& Add(float);
public:
	friend std::ostream& operator <<(std::ostream&, const Rnd &);
	int getN() const;
	Rnd();
	Rnd(int);
	Rnd(int, float *);
	Rnd(const Rnd&);
	Rnd(Rnd&&);
	Rnd& operator =(const Rnd&);
	~Rnd();
	int Renew();
	const Rnd operator ++(int);
	float operator[](const int) const;
	double Average() const;
	double Mediana() const;
	float Razmax() const;
	double Disp() const;
	Rnd& operator ~ ();
	Rnd Rnd::operator()(float,float) const;
};

