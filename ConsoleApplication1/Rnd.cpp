#include "stdafx.h"
#include <time.h>
#include "Rnd.h"

std::ostream& operator <<(std::ostream&p, const Rnd &r)
{
	if (r.n == 0)
		p << "� ������� ������ ���" << std::endl;
	else
		for (int i = 0; i < r.n; i++)
			p << r.m[i] << std::endl;;
	return p;
}

int compare(const void * x1, const void * x2)   // ������� ��������� ��������� �������
{
	return (*(int*)x1 - *(int*)x2);              // ���� ��������� ��������� ����� 0, �� ����� �����, < 0: x1 < x2; > 0: x1 > x2
}

int Rnd::getN() const
{
	return n;
}

Rnd::Rnd() :
n(0), m(NULL){ }

Rnd::Rnd(int n)
{
	if (n < 1)
		throw ("Invalid size");
	else
	{
		this->n = n;
        m = new float[n];
		srand(time(NULL));
		for (int i = 0; i < n; i++)
		{
			m[i] = (float)(rand()) / RAND_MAX;
		}
	}
}

Rnd::Rnd(int n, float *mas)
{
	if (n < 1)
		throw ("Invalid size");
	else
	{
		this->n = n;
		m = new float[n];
		for (int i = 0; i < n; i++)
		{
			m[i] = mas[i];
		}
	}
}

Rnd::Rnd(const Rnd &obj) :n(obj.n), m(NULL)
{
	if (n){
		m = new float[n];
		for (int i = 0; i < n; i++)
			m[i] = obj.m[i];
	}
}

Rnd::Rnd(Rnd&&obj) :n(obj.n), m(obj.m)
{
	std::cout << "&&";
	obj.m = NULL;
}

Rnd & Rnd::operator = (const Rnd& obj)
{
	if (this != &obj)
	{
		delete[] m;
		m = NULL;
		if ((n = obj.n) != 0)
		{
			m = new float[n];
			for (int i = 0; i < n; i++)
				m[i] = obj.m[i];
		}
	}
	return *this;
}

Rnd:: ~Rnd()
{
	delete[] m;
	m = NULL;
}

int Rnd::Renew()
{
	for (int i = 0; i < n; ++i)
		m[i] = (float)(rand()) / RAND_MAX;
	return 1;
}

const Rnd Rnd::operator ++(int)
{
	Rnd x(*this);
	delete[]m;
	m = NULL;
	m = new float[n + 1];
	for (int i = 0; i < n; i++)
		m[i] = x.m[i];
	m[n++] = (float)(rand()) / RAND_MAX;
	return x;
}

float Rnd::operator[] (int i) const
{
	if (i < 0 | i + 1 > n) //������������ ������ �� ������� ������ n - ���������� ����� � �������
		throw ("Illegal index");
	return m[i];
}

double Rnd::Average() const
{
	double s = 0;
	if (n == 0)
		return 0;
	for (int i = 0; i < n; i++)
		s += m[i];
	return s / n;
}

double Rnd::Mediana() const
{
	double *a = new double[n];
	for (int i = 0; i < n; i++)
		a[i] = m[i];
	qsort(a, n, sizeof(double), compare);
	std::cout << std::endl;
	int k = n % 2;
	double m;
	if (k == 1) m=a[(n - 1) / 2];
	if (k == 0) m= 0.5*(a[n / 2] + a[(n / 2)-1]);
	delete[]a;
	return m;
}

float Rnd::Razmax()const
{
float *a = new float[n];
for (int i = 0; i < n; i++)
	a[i] = m[i];
qsort(a, n, sizeof(float), compare);
float k = a[n - 1] - a[0];
delete[]a;
return k;
}

double Rnd::Disp()const
{
	double s = 0;
	double avg = this->Average();
	for (int i = 0; i < n; i++)
	{
		s += pow(m[i]-avg,2);
	}
	return s / n;
}

Rnd& Rnd::operator~()
{
	for (int i = 0; i < n; i++)
		m[i] = 1 - m[i];
	return *this;
}

Rnd& Rnd::Add(float k)
{
	Rnd x(*this);
	delete[]m;
	m = NULL;
	m = new float[n + 1];
	for (int i = 0; i < n; i++)
		m[i] = x.m[i];
	m[n++] = k;
	return *this;
}

Rnd Rnd::operator () (float x, float y) const
{
	Rnd a;
	for (int i = 0; i < n; i++)
		if (m[i] >= x&m[i] <= y)
			a.Add(m[i]);
	return a;
}