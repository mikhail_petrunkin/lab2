// Testing.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include "gtest\gtest.h"
#include "..\ConsoleApplication1\Rnd.h"

//ASSERT_ANY_THROW(a.xy(-50));2
//EXPECT_DOUBLE_EQ(5, a.xy(0));

TEST(ConstructionTesting, DefaultConstructor)
{
	Rnd a;
	EXPECT_DOUBLE_EQ(0,a.getN());
}

TEST(ConstructionTesting, LConstructor)
{
	Rnd a(20);
	EXPECT_DOUBLE_EQ(20, a.getN());
	ASSERT_ANY_THROW(Rnd b(-5));
}


TEST(ConstructionTesting, MConstructor)
{
	float a[]{0.9, 0.5, 0.6, 0.1, 0.07, 0.7, 0.0, 1, 0.96, 0.8};
	Rnd b (10,a);
	for (int i = 0; i < 10;i++)
	EXPECT_DOUBLE_EQ(a[i], b[i]);
}

TEST(ConstructionTesting, CopyConstructor)
{
	Rnd a(500);
	Rnd b(a);
	for (int i = 0; i < 500; i++)
		EXPECT_DOUBLE_EQ(a[i], b[i]);
}

TEST(ConstructionTesting, AssignConstructor)
{
	Rnd a(550);
	Rnd b(220);
	b = a;
	for (int i = 0; i < 550; i++)
		EXPECT_DOUBLE_EQ(a[i], b[i]);
}


TEST(RndTesting, Renew)
{
	Rnd a(5);
	a.Renew();
	EXPECT_DOUBLE_EQ(5, a.getN());
}

TEST(RndTesting, Add)
{
	Rnd a(5);
	a++;
	EXPECT_DOUBLE_EQ(6, a.getN());
}

TEST(RndTesting, Select)
{
	float a[]{0.9, 0.5, 0.6, 0.1, 0.07, 0.7, 0.0, 1, 0.96, 0.8};
	Rnd b(10, a);
	EXPECT_NEAR(0.9,b[0] ,0.001);
	EXPECT_NEAR(0.8,b[9], 0.001);
	ASSERT_ANY_THROW(b[10]);
	ASSERT_ANY_THROW(b[-1]);
}

TEST(RndTesting, Average)
{
	float a[]{0.9, 0.5, 0.6, 0.1, 0.07, 0.7, 0.0, 1, 0.96, 0.8};
	Rnd b(10, a);
	Rnd c;
	EXPECT_DOUBLE_EQ(0, c.Average());
	EXPECT_NEAR(0.563, b.Average(),0.001);
}

TEST(RndTesting, 1Addition)
{
	Rnd b(80);
	Rnd c = b;
	~c;
	for (int i = 0; i < 80; i++)
		EXPECT_NEAR(b[i]+c[i],1, 0.001);
}

TEST(RndTesting, Choice)
{
	float l1 = (float)(rand()) / RAND_MAX;
	float l2 = l1 + 0.3;
	const int SIZE2 = 9000;
	float *m1 = new float[SIZE2];
	for (int i = 0; i < SIZE2;i++)
		m1[i] = (float)(rand()) / RAND_MAX;
	float *m2 = new float[SIZE2];
	int k = 0;
		for (int i = 0; i < SIZE2;i++)
			if (m1[i] >= l1&m1[i] <= l2)
			{
				m2[k] = m1[i];
				k++;
			}
		delete []m1;
		Rnd d1(SIZE2, m2);
		Rnd d2(d1(l1, l2));
		for (int i = 0; i < k;i++)
			EXPECT_NEAR(d2[i], m2[i], 0.001);
}

int _tmain(int argc, _TCHAR* argv[])
{
	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
	return 0;
}